/*
Copyright (c) Daybrush
name: @scenejs/render
license: MIT
author: Daybrush
repository: git+https://github.com/daybrush/scenejs-render.git
version: 0.10.4
*/
'use strict';

var puppeteer = require('puppeteer');
require('fs');

/*! *****************************************************************************
Copyright (c) Microsoft Corporation. All rights reserved.
Licensed under the Apache License, Version 2.0 (the "License"); you may not use
this file except in compliance with the License. You may obtain a copy of the
License at http://www.apache.org/licenses/LICENSE-2.0

THIS CODE IS PROVIDED ON AN *AS IS* BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION ANY IMPLIED
WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE,
MERCHANTABLITY OR NON-INFRINGEMENT.

See the Apache Version 2.0 License for specific language governing permissions
and limitations under the License.
***************************************************************************** */
function __awaiter(thisArg, _arguments, P, generator) {
  return new (P || (P = Promise))(function (resolve, reject) {
    function fulfilled(value) {
      try {
        step(generator.next(value));
      } catch (e) {
        reject(e);
      }
    }

    function rejected(value) {
      try {
        step(generator["throw"](value));
      } catch (e) {
        reject(e);
      }
    }

    function step(result) {
      result.done ? resolve(result.value) : new P(function (resolve) {
        resolve(result.value);
      }).then(fulfilled, rejected);
    }

    step((generator = generator.apply(thisArg, _arguments || [])).next());
  });
}
function __generator(thisArg, body) {
  var _ = {
    label: 0,
    sent: function () {
      if (t[0] & 1) throw t[1];
      return t[1];
    },
    trys: [],
    ops: []
  },
      f,
      y,
      t,
      g;
  return g = {
    next: verb(0),
    "throw": verb(1),
    "return": verb(2)
  }, typeof Symbol === "function" && (g[Symbol.iterator] = function () {
    return this;
  }), g;

  function verb(n) {
    return function (v) {
      return step([n, v]);
    };
  }

  function step(op) {
    if (f) throw new TypeError("Generator is already executing.");

    while (_) try {
      if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
      if (y = 0, t) op = [op[0] & 2, t.value];

      switch (op[0]) {
        case 0:
        case 1:
          t = op;
          break;

        case 4:
          _.label++;
          return {
            value: op[1],
            done: false
          };

        case 5:
          _.label++;
          y = op[1];
          op = [0];
          continue;

        case 7:
          op = _.ops.pop();

          _.trys.pop();

          continue;

        default:
          if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) {
            _ = 0;
            continue;
          }

          if (op[0] === 3 && (!t || op[1] > t[0] && op[1] < t[3])) {
            _.label = op[1];
            break;
          }

          if (op[0] === 6 && _.label < t[1]) {
            _.label = t[1];
            t = op;
            break;
          }

          if (t && _.label < t[2]) {
            _.label = t[2];

            _.ops.push(op);

            break;
          }

          if (t[2]) _.ops.pop();

          _.trys.pop();

          continue;
      }

      op = body.call(thisArg, _);
    } catch (e) {
      op = [6, e];
      y = 0;
    } finally {
      f = t = 0;
    }

    if (op[0] & 5) throw op[1];
    return {
      value: op[0] ? op[1] : void 0,
      done: true
    };
  }
}

function sendMessage(message) {
  process.send && process.send(message);
}
function caputreLoop(_a) {
  var isOnlyMedia = _a.isOnlyMedia,
      page = _a.page,
      name = _a.name,
      delay = _a.delay,
      fps = _a.fps,
      media = _a.media,
      isMedia = _a.isMedia,
      playSpeed = _a.playSpeed,
      startFrame = _a.startFrame,
      endFrame = _a.endFrame,
      endTime = _a.endTime,
      totalFrame = _a.totalFrame;
  return __awaiter(this, void 0, void 0, function () {
    function loop(frame) {
      return __awaiter(this, void 0, void 0, function () {
        var time, _a, _b;

        return __generator(this, function (_c) {
          switch (_c.label) {
            case 0:
              time = Math.min(frame * playSpeed / fps, endTime);
              console.log("Capture frame: " + frame + ", time: " + time);
              _a = !isOnlyMedia;
              if (!_a) return [3
              /*break*/
              , 2];
              return [4
              /*yield*/
              , page.evaluate(name + ".setTime(" + (time - delay) + ", true)")];

            case 1:
              _a = _c.sent();
              _c.label = 2;

            case 2:
              _b = isMedia;
              if (!_b) return [3
              /*break*/
              , 4];
              return [4
              /*yield*/
              , page.evaluate(media + ".setTime(" + time + ")")];

            case 3:
              _b = _c.sent();
              _c.label = 4;

            case 4:
              return [4
              /*yield*/
              , new Promise(function (r) {
                return setTimeout(r, 600);
              })];

            case 5:
              _c.sent();

              return [4
              /*yield*/
              , page.screenshot({
                path: "./.scene_cache/frame" + frame + ".jpg",
                type: "jpeg",
                quality: 100
              })];

            case 6:
              _c.sent();

              sendMessage({
                type: "capture",
                frame: frame,
                totalFrame: totalFrame
              });

              if (time === endTime || frame >= endFrame) {
                return [2
                /*return*/
                ];
              }

              return [4
              /*yield*/
              , new Promise(function (r) {
                return setTimeout(r, 600);
              })];

            case 7:
              _c.sent();

              return [4
              /*yield*/
              , loop(frame + 1)];

            case 8:
              _c.sent();

              return [2
              /*return*/
              ];
          }
        });
      });
    }

    return __generator(this, function (_b) {
      switch (_b.label) {
        case 0:
          return [4
          /*yield*/
          , loop(startFrame)];

        case 1:
          _b.sent();

          return [2
          /*return*/
          ];
      }
    });
  });
}
function openPage(_a) {
  var browser = _a.browser,
      width = _a.width,
      height = _a.height,
      path = _a.path,
      scale = _a.scale,
      name = _a.name,
      media = _a.media,
      referer = _a.referer;
  return __awaiter(this, void 0, void 0, function () {
    var page, e_1, e_2;
    return __generator(this, function (_b) {
      switch (_b.label) {
        case 0:
          return [4
          /*yield*/
          , browser.newPage()];

        case 1:
          page = _b.sent();
          page.setUserAgent(browser.userAgent() + " Scene.js");
          page.setViewport({
            width: width / scale,
            height: height / scale,
            deviceScaleFactor: scale
          });
          return [4
          /*yield*/
          , page.goto(path, {
            referer: referer
          })];

        case 2:
          _b.sent();

          _b.label = 3;

        case 3:
          _b.trys.push([3, 5,, 6]);

          return [4
          /*yield*/
          , page.evaluate(name + ".finish()")];

        case 4:
          _b.sent();

          return [3
          /*break*/
          , 6];

        case 5:
          e_1 = _b.sent();
          return [3
          /*break*/
          , 6];

        case 6:
          _b.trys.push([6, 8,, 9]);

          return [4
          /*yield*/
          , page.evaluate(media + ".finish()")];

        case 7:
          _b.sent();

          return [3
          /*break*/
          , 9];

        case 8:
          e_2 = _b.sent();
          return [3
          /*break*/
          , 9];

        case 9:
          return [2
          /*return*/
          , page];
      }
    });
  });
}

var _this = undefined;

function capture(_a) {
  var name = _a.name,
      media = _a.media,
      path = _a.path,
      endTime = _a.endTime,
      fps = _a.fps,
      width = _a.width,
      height = _a.height,
      scale = _a.scale,
      delay = _a.delay,
      playSpeed = _a.playSpeed,
      startFrame = _a.startFrame,
      endFrame = _a.endFrame,
      totalFrame = _a.totalFrame,
      isMedia = _a.isMedia,
      referer = _a.referer;
  return __awaiter(this, void 0, void 0, function () {
    var browser, page;
    return __generator(this, function (_b) {
      switch (_b.label) {
        case 0:
          return [4
          /*yield*/
          , puppeteer.launch({
            args: ['--no-sandbox', '--disable-setuid-sandbox'],
            headless: true
          })];

        case 1:
          browser = _b.sent();
          return [4
          /*yield*/
          , openPage({
            browser: browser,
            width: width,
            height: height,
            path: path,
            scale: scale,
            name: name,
            media: media,
            referer: referer
          })];

        case 2:
          page = _b.sent();
          console.log("Start SubCapture (startFrame: " + startFrame + ", endFrame: " + endFrame + ")");
          return [4
          /*yield*/
          , caputreLoop({
            page: page,
            name: name,
            fps: fps,
            delay: delay,
            media: media,
            isMedia: isMedia,
            playSpeed: playSpeed,
            startFrame: startFrame,
            endFrame: endFrame,
            endTime: endTime,
            totalFrame: totalFrame,
            isOnlyMedia: false
          })];

        case 3:
          _b.sent();

          return [4
          /*yield*/
          , browser.close()];

        case 4:
          _b.sent();

          return [2
          /*return*/
          ];
      }
    });
  });
}

process.on("message", function (data) {
  return __awaiter(_this, void 0, void 0, function () {
    return __generator(this, function (_a) {
      switch (_a.label) {
        case 0:
          return [4
          /*yield*/
          , capture(data)];

        case 1:
          _a.sent();

          process.exit();
          return [2
          /*return*/
          ];
      }
    });
  });
});
//# sourceMappingURL=subcapture.js.map
